"""Ball class"""

from turtle import Turtle


class Ball(Turtle):
    """Ball class"""

    def __init__(self):
        """Init ball"""
        super().__init__()
        self.shape("circle")
        self.color("white")
        self.penup()
        self.goto(0, 0)
        self.x_move = 10
        self.y_move = 10

    def move(self):
        """Move the ball"""
        new_x = self.xcor() + self.x_move
        new_y = self.ycor() + self.y_move
        self.goto(new_x, new_y)

    def bounce_y(self):
        """Bounce the ball on Y axis"""
        self.y_move *= -1

    def bounce_x(self):
        """Bounce the ball on X axis"""
        self.x_move *= -1

    def restart(self):
        """Place ball at the center and change direction"""
        self.bounce_x()
        self.goto(0, 0)
