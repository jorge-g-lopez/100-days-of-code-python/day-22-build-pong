"""Pong"""

import time
from turtle import Screen

from ball import Ball
from paddle import Paddle
from scoreboard import Scoreboard

screen = Screen()
screen.setup(800, 600)
screen.bgcolor("black")
screen.title("Pong")
screen.tracer(0)

l_paddle = Paddle((-350, 0))
r_paddle = Paddle((350, 0))
ball = Ball()
scoreboard = Scoreboard()

screen.listen()
screen.onkey(r_paddle.up, "Up")
screen.onkey(r_paddle.down, "Down")
screen.onkey(l_paddle.up, "w")
screen.onkey(l_paddle.down, "x")

game_is_on = True
sleep_time = 0.1

while game_is_on:
    time.sleep(sleep_time)
    ball.move()
    screen.update()

    if ball.ycor() >= 280 or ball.ycor() <= -280:
        ball.bounce_y()

    if (ball.distance(r_paddle) < 50 and ball.xcor() > 320) or (
        ball.distance(l_paddle) < 50 and ball.xcor() < -320
    ):
        ball.bounce_x()
        sleep_time -= 0.005

    if ball.xcor() > 380:
        scoreboard.l_point()
        sleep_time = 0.1
        time.sleep(1)
        ball.restart()

    if ball.xcor() < -380:
        scoreboard.r_point()
        sleep_time = 0.1
        time.sleep(1)
        ball.restart()

screen.exitonclick()
