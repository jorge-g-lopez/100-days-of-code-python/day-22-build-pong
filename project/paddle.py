"""Paddle class"""

from turtle import Turtle


class Paddle(Turtle):
    """Paddle class"""

    def __init__(self, initial_position):
        """Init paddle with parameter initial_position as (x,y)"""
        super().__init__()
        self.shape("square")
        self.color("white")
        self.penup()
        self.shapesize(5, 1)
        self.goto(initial_position)

    def up(self):
        """Move the paddle up"""
        self.goto(self.xcor(), self.ycor() + 20)

    def down(self):
        """Move the paddle down"""
        self.goto(self.xcor(), self.ycor() - 20)
