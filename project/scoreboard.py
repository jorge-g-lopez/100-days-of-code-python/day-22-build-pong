"""Scoreboard class"""

from turtle import Turtle


class Scoreboard(Turtle):
    """Scoreboard class"""

    def __init__(self):
        """Init scoreboard"""
        super().__init__()
        self.hideturtle()
        self.color("white")
        self.penup()
        self.lscore = 0
        self.rscore = 0
        self.update_scoreboard()

    def update_scoreboard(self):
        """Update the scoreboard"""
        self.clear()
        self.goto(-100, 200)
        self.write(self.lscore, align="center", font=("Courier", 80, "normal"))
        self.goto(100, 200)
        self.write(self.rscore, align="center", font=("Courier", 80, "normal"))

    def l_point(self):
        """Add a point to the left paddle"""
        self.lscore += 1
        self.update_scoreboard()

    def r_point(self):
        """Add a point to the right paddle"""
        self.rscore += 1
        self.update_scoreboard()
